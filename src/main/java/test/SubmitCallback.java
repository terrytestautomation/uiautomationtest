package test;
import org.testng.annotations.Test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import io.github.sukgu.*;


public class SubmitCallback {
	
	WebDriver driver;
	WebDriverWait wait20;
	
	static final String SOURCE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static final String numberList="0123456789";
	
	//define callback form test data
	String fName = "Test" + randomString(4).toLowerCase();
	String lName = randomString(3).toUpperCase();
	String phoneNumber = "04"+ randomNumber(8);
	String email = fName + "."+lName +"@gmail.com";
	
	List<WebElement> optionList;
	Random r = new Random();
	int indexR = 0;
	
	@BeforeClass
	public void testSetp()
	{
		//setting system properties of ChromeDriver
		System.setProperty("webdriver.chrome.driver", ".\\Driver\\chromedriver.exe");
		
		//Creating an object of ChromeDriver
		driver= new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		wait20 = new WebDriverWait(driver, 20);
		
		
	}
	
	@BeforeMethod
	public void openBrowser() throws Exception
	{
	//launch NAB home page
	driver.get("https://www.nab.com.au");
	
	}
	

	@Test(description="This method validates submit the new home loan callback form")
	public void submitCallbackForm() throws Exception
	{
		
		wait20.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Personal")));
		
		driver.findElement(By.linkText("Personal")).click();
		System.out.println("We are currently on the following URL" +driver.getCurrentUrl());
		
		wait20.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Home loans')]")));
		driver.findElement(By.xpath("//span[contains(text(),'Home loans')]")).click();
		
		wait20.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='parent']//a[@href='/personal/home-loans']")));
		
		driver.findElement(By.xpath("//li[@class='parent']//a[@href='/personal/home-loans']")).click();
		
		
		/*
		 * wait20.until(ExpectedConditions.visibilityOfElementLocated(By.
		 * xpath("//ul[@class='link-list linklist']//li[2]")));
		 * driver.findElement(By.xpath("//ul[@class='link-list linklist']//li[2]")).
		 * click();
		 */
		wait20.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Request a call back')]")));
		driver.findElement(By.xpath("//span[contains(text(),'Request a call back')]")).click();
		
		//locate shadow New Home Loans radio button
		Shadow shadow = new Shadow(driver);
	      shadow.findElement("label[data-component-id='Radio']").click();
	      
	      WebElement element=shadow.findElement("button[data-component-id='Button']");

	      shadow.scrollTo(element);
	      
	      element.click();
	      
		//Switch to new call back form tab
		  String parentWindow = driver.getWindowHandle();
		  
		  Set <String> windows = driver.getWindowHandles();
		  
		  for (String window:windows) 
		  { 
			  if (!window.equals(parentWindow)) {
		  driver.switchTo().window(window); 
			  } 
		  }
		
		Thread.sleep(2000);
		
		//start fill the form
		driver.findElement(By.xpath("//span[contains(text(),'No')]")).click();
		
		
		driver.findElement(By.xpath("//input[@id='field-page-Page1-aboutYou-firstName']")).sendKeys(fName);
		
		driver.findElement(By.xpath("//input[@id='field-page-Page1-aboutYou-lastName']")).sendKeys(lName);
		
		driver.findElement(By.xpath("//div[@class='css-1hwfws3 react-select__value-container']")).click();
	    
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//div[@id='react-select-3-option-1']")).click();
		
		driver.findElement(By.xpath("//input[@id='field-page-Page1-aboutYou-phoneNumber']")).sendKeys(phoneNumber);
		driver.findElement(By.xpath("//input[@id='field-page-Page1-aboutYou-email']")).sendKeys(email);
		
		//scroll down the form page to click on Submit button
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1000);");
		
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//button[@id='page-Page1-btnGroup-submitBtn']")).click();
		
		String expectValue = "WE'VE RECEIVED YOUR REQUEST";
		
		String receivedTxt = driver.findElement(By.xpath("//h1[contains(text(),'RECEIVED YOUR REQUEST')]")).getText();
		//validate the text message after the form is submitted
		Assert.assertTrue(receivedTxt.equalsIgnoreCase(expectValue),
				"Callback form submission is not successful");
		
	}
	
	@AfterMethod
	public void terminateBrowser() {
		driver.close();
	}
	
	//get random test data
	public static String randomString(int length) {
		StringBuilder sb = new StringBuilder(length);
		Random testr=new Random();
		for (int i = 0; i < length; i++) 
			{
				sb.append(SOURCE.charAt(testr.nextInt(SOURCE.length()))); 
			}
		
		return sb.toString(); 
	}
	
	public static String randomNumber(int length)
	{
		StringBuilder sb=new StringBuilder(length);
		Random testr=new Random();
		for(int i=0;i<length;i++)
		{
			sb.append(numberList.charAt(testr.nextInt(numberList.length())));
		}
		
		return sb.toString();
	}
	

	
}
